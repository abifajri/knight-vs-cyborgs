extends Node2D

var score = 0

const enemy_prefab = preload("res://Scenes/Enemy1.tscn")
const potion_prefab = preload("res://Scenes/Potion.tscn")
onready var player = get_node("/root/Main/Player")

onready var hp_label = $HPContainer/HP
onready var score_label = $ScoreContainer/Score
onready var buff_label = $BuffContainer/Buff

var is_spawnable_left = true
var	spawner_on_cooldown_left = false
var is_spawnable_right = true
var	spawner_on_cooldown_right = false

var cannot_spawn_potion = true

func scoring():
	score += 100
	print(score)

func _ready( ):
	randomize()
	
func _process(delta):
	if is_instance_valid(player):
		hp_label.text = "HP: " + str(player.hp)
		if player.hp <= 50:
			buff_label.text = "Buff Activated!"
		elif player.hp > 50:
			buff_label.text = ""
	else:
		hp_label.text = "HP: 0"
	score_label.text = str(score)
	
	if len(get_tree().get_nodes_in_group("Enemy")) < 7 and spawner_on_cooldown_left:
		is_spawnable_left = false
	elif len(get_tree().get_nodes_in_group("Enemy")) < 7 and !spawner_on_cooldown_left:
		is_spawnable_left = true
	elif len(get_tree().get_nodes_in_group("Enemy")) >= 8:
		is_spawnable_left = false
		
	if is_spawnable_left:
		var enemy = enemy_prefab.instance()
		add_child(enemy)
		enemy.position = $SpawnerEnemyLeft.position
		spawner_on_cooldown_left = true
		spawner_on_cooldown_right = true
		yield(get_tree().create_timer(2.5), "timeout")
		spawner_on_cooldown_left = false
		spawner_on_cooldown_right = false
	
	if len(get_tree().get_nodes_in_group("Enemy")) < 8 and spawner_on_cooldown_right:
		is_spawnable_right = false
	elif len(get_tree().get_nodes_in_group("Enemy")) < 8 and !spawner_on_cooldown_right:
		is_spawnable_right = true
	elif len(get_tree().get_nodes_in_group("Enemy")) >= 8:
		is_spawnable_right = false
	
	if is_spawnable_right:
		var enemy = enemy_prefab.instance()
		add_child(enemy)
		enemy.position = $SpawnerEnemyRight.position
		spawner_on_cooldown_left = true
		spawner_on_cooldown_right = true
		yield(get_tree().create_timer(2.5), "timeout")
		spawner_on_cooldown_left = false
		spawner_on_cooldown_right = false
		
	if !cannot_spawn_potion:
		var potion = potion_prefab.instance()
		add_child(potion)
		var position = rand_range(0, 30)
		if position < 10:
			potion.position = $SpawnerPotionLeft.position
		elif position >= 10 and position <= 20:
			potion.position = $SpawnerPotionCenter.position
		elif position > 20:
			potion.position = $SpawnerPotionRight.position
		$SpawnerPotionTimer.start()
		cannot_spawn_potion = true


func _on_SpawnerPotionTimer_timeout():
	cannot_spawn_potion = false
