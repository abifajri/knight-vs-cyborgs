extends KinematicBody2D

var GRAVITY = 800
var movement = Vector2()
const RUN_SPEED = 80

onready var main = get_node("/root/Main")
onready var player = get_node("/root/Main/Player")

var hp = 80
var is_alive = true
var is_hit = false
var is_attacking = false
var knockback = 25
var facing = 'right'

func _process(delta):
	if is_alive and hp <= 0:
		die()
		
	if is_alive and !is_hit and !is_attacking:
		if movement.x != 0:
			if movement.x < 0:
				$Enemy1Sprite.flip_h = true
				$Enemy1Sprite.offset.x = -10;
				$AttackArea/AttackCollision.transform.origin = Vector2(-16.5, 2);
				$PlayerDetectionArea/PlayerDetectionCollision.transform.origin = Vector2(-16.5, 2);
			if movement.x > 0:
				$Enemy1Sprite.flip_h = false
				$Enemy1Sprite.offset.x = 10;
				$AttackArea/AttackCollision.transform.origin = Vector2(16.5, 2);
				$PlayerDetectionArea/PlayerDetectionCollision.transform.origin = Vector2(16.5, 2);
			$Enemy1Sprite.play("Run")
		else:
			$Enemy1Sprite.play("Idle")
			
		if is_instance_valid(player):
			movement = (player.position - position).normalized()
		else:
			movement.x = 0
		if not is_on_floor():
			movement.y += delta * GRAVITY;
		
		movement = move_and_slide(movement * RUN_SPEED, Vector2.UP);

func die():
	movement.x = 0
	is_alive = false
	main.scoring()
	$Enemy1Sprite.play("Death")
	
func attack():
	$AttackResetTimer.start();
	is_attacking = true;
	$Enemy1Sprite.play("Attack");
	$PlayerDetectionArea/PlayerDetectionCollision.disabled = true;

func _on_Enemy1Hitbox_area_entered(area):
	if (area.is_in_group("Attack") or area.is_in_group("HeavyAttack")) and is_alive:
		if area.is_in_group("Attack"):
			hp -= Global.Player_damage
#			print("attack")
		elif area.is_in_group("HeavyAttack"):
			hp -= Global.Player_heavy_damage
#			print("heavy")
		$Enemy1Sprite.speed_scale = 0.75
		$Enemy1Sprite.play("Hit")
		is_hit = true
		yield(get_tree().create_timer(0.7), "timeout")
		is_hit = false
		$Enemy1Sprite.speed_scale = 1.5

func _on_Enemy1Sprite_animation_finished():
	if $Enemy1Sprite.animation == "Death":
		queue_free()

func _on_AttackResetTimer_timeout():
	is_attacking = false;
	$AttackArea/AttackCollision.disabled = false;
	$PlayerDetectionArea/PlayerDetectionCollision.disabled = false;

func _on_PlayerDetectionArea_area_entered(area):
	if area.is_in_group("Player") and is_alive and not is_attacking:
#		print("player detected")
		$AttackArea/AttackCollision.disabled = false;
		attack()
