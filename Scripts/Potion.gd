extends RigidBody2D

onready var player = get_node("/root/Main/Player")

func _on_PotionArea_area_entered(area):
	if area.is_in_group("Player"):
		player.set_hp(player.get_hp() + 50)
		queue_free()
