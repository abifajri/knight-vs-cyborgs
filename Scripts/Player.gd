extends KinematicBody2D

var GRAVITY = 800;
var speed = 250;
var dash_speed = 350;
var can_dash = false;
var is_dashing = false;

var movement = Vector2();
var facing = "right";

var JUMP_FORCE = 350;
var is_jumping = false;

var AttackPoints = 2;
var isAttacking = false;
var is_heavy_attacking = false;

var hp = 100;
var is_alive = true;
var is_hit = false;

onready var main = get_node("/root/Main")

func _ready():
	$PlayerCamera.limit_right = 358
	$PlayerCamera.limit_left = 150

func _process(delta):
	if hp <= 50:
		Global.Player_damage = 15
		Global.Player_heavy_damage = 37.5
	if hp > 50:
		Global.Player_damage = 10
		Global.Player_heavy_damage = 25
	
	if hp <= 0:
		is_alive = false;
	
	
	if is_alive:
		get_input();
	else:
		die();
	
	movement.y += delta * GRAVITY;
	movement = move_and_slide(movement, Vector2.UP);

func get_hp():
	return hp

func set_hp(new_hp):
	hp = new_hp
	if hp > 100:
		hp = 100

func hit():
	movement.x = 0
	$PlayerSprite.play("Hit")
	hp -= Global.Enemy1_damage
	print("player hp", hp)
	is_hit = true
	yield(get_tree().create_timer(0.5), "timeout")
	is_hit = false
	is_heavy_attacking = false

func dash():
	can_dash = true if is_on_floor() else false;
	
	if Input.is_action_just_pressed("Dash") and can_dash and !is_heavy_attacking:
		if facing == "right":
			movement.x = dash_speed;
		elif facing == "left":
			movement.x = -dash_speed;
		$DashTimer.start();
		$PlayerSprite.speed_scale = 6.5;
		$PlayerSprite.play("Dash");
		is_dashing = true;
		can_dash = false;
		set_collision_mask_bit(2, false)
		$PlayerHitbox/HitboxCollision.disabled = true
		
func flip_sprite():
	if facing == "right":
		$PlayerSprite.flip_h = false;
		$PlayerCollision.transform.origin = Vector2(0, 2.5);
		$PlayerHitbox/HitboxCollision.transform.origin = Vector2(0, 2.5);
		$AttackArea/AttackCollision.transform.origin = Vector2(30, -1.5);
		$HeavyAttackArea/AttackCollision.transform.origin = Vector2(30, -1.5);
	elif facing == "left":
		$PlayerSprite.flip_h = true;
		$PlayerCollision.transform.origin = Vector2(10, 2.5);
		$PlayerHitbox/HitboxCollision.transform.origin = Vector2(10, 2.5);
		$AttackArea/AttackCollision.transform.origin = Vector2(-20, -1.5);
		$HeavyAttackArea/AttackCollision.transform.origin = Vector2(-20, -1.5);

func get_input():
	flip_sprite();
	
	# jump input here
	if is_on_floor():
		is_jumping = false;
	
	if Input.is_action_just_pressed("Jump") and is_on_floor() and !is_heavy_attacking and !is_dashing:
		movement.y -= JUMP_FORCE;
		is_jumping = true;
	
	if !is_on_floor() and movement.y < 0:
		$PlayerSprite.play("Jump_up");
	elif !is_on_floor() and movement.y > 0:
		$PlayerSprite.play("Jump_down");
	
	# movement input here
	if Input.is_action_pressed("Right") && !isAttacking && !is_dashing and !is_heavy_attacking:
		movement.x = speed;
		facing = "right";
		if is_on_floor():
			$PlayerSprite.play("Run");
	elif Input.is_action_pressed("Left") && !isAttacking && !is_dashing and !is_heavy_attacking:
		movement.x = -speed;
		facing = "left";
		if is_on_floor():
			$PlayerSprite.play("Run");
	elif !is_dashing and !is_hit:
		movement.x = 0;
		if !isAttacking and !is_jumping and !is_heavy_attacking:
			$PlayerSprite.play("Idle");
	
	# attack input Here
	if Input.is_action_just_pressed("Attack") && AttackPoints == 2 && !is_dashing and !is_heavy_attacking and !is_jumping:
		$AttackArea/AttackCollision.disabled = true;
		$AttackArea/AttackSFX.playing = true
		$AttackResetTimer.start();
		$PlayerSprite.speed_scale = 2.5;
		$PlayerSprite.play("Attack1");
		AttackPoints -= 1;
		isAttacking = true;
		$AttackArea/AttackCollision.disabled = false;
	elif Input.is_action_just_pressed("Attack") && AttackPoints == 1 && !is_dashing and !is_heavy_attacking and !is_jumping:
		$AttackArea/AttackCollision.disabled = true;
		$AttackArea/AttackSFX.playing = true
		$AttackResetTimer.start();
		$PlayerSprite.play("Attack2");
		AttackPoints -= 1;
		isAttacking = true;
		$AttackArea/AttackCollision.disabled = false;
	elif Input.is_action_just_pressed("HeavyAttack") and !is_dashing and !is_heavy_attacking and !is_jumping:
		$HeavyAttackArea/AttackCollision.disabled = true;
		$AttackArea/AttackSFX.playing = true
		$AttackResetTimer.start();
		$PlayerSprite.speed_scale = 1.5;
		$PlayerSprite.play("HeavyAttack");
		isAttacking = true;
		is_heavy_attacking = true;
		$HeavyAttackArea/AttackCollision.disabled = false;
	
	dash();

func die():
	movement.x = 0
	is_alive = true
	$PlayerSprite.play("Death")

func _on_PlayerSprite_animation_finished():
	if $PlayerSprite.animation == "Attack1":
		$PlayerSprite.play("Idle");
		isAttacking = false;
		$AttackArea/AttackCollision.disabled = true;
	elif $PlayerSprite.animation == "Attack2":
		$PlayerSprite.play("Idle");
		isAttacking = false;
		$AttackArea/AttackCollision.disabled = true;
		AttackPoints = 2;
	elif $PlayerSprite.animation == "HeavyAttack":
		$PlayerSprite.speed_scale = 2.5;
		$PlayerSprite.play("Idle");
		isAttacking = false;
		is_heavy_attacking = false;
		$HeavyAttackArea/AttackCollision.disabled = true;
		AttackPoints = 2;
	elif $PlayerSprite.animation == "Dash":
		$PlayerSprite.play("Idle");
		$PlayerSprite.speed_scale = 2.5;
		is_dashing = false;
		set_collision_mask_bit(2, true)
		$PlayerHitbox/HitboxCollision.disabled = false
	elif $PlayerSprite.animation == "Death":
		Global.score = main.score
		get_tree().change_scene("res://Scenes/GameOver.tscn")
		
		
func _on_AttackResetTimer_timeout():
	AttackPoints = 2;
	isAttacking = false;
	$AttackArea/AttackCollision.disabled = true;
	$HeavyAttackArea/AttackCollision.disabled = true;

func _on_DashTimer_timeout():
	can_dash = true;
	is_dashing = false;
	set_collision_mask_bit(2, true)
	$PlayerHitbox/HitboxCollision.disabled = false

func _on_PlayerHitbox_area_entered(area):
	if !is_alive:
		return
	if area.is_in_group("EnemyAttack") and is_alive and not is_hit and not is_dashing:
		hit()
